package ru.yandex.dimax.javatemplate.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;
import ru.yandex.dimax.javatemplate.dto.RegisterUserDto;
import ru.yandex.dimax.javatemplate.dto.UserDto;
import ru.yandex.dimax.javatemplate.model.Gender;
import ru.yandex.dimax.javatemplate.model.User;

@Mapper(componentModel = "spring")
public interface UserMapper {

    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    @Mapping(target = "email", source = "email")
    @Mapping(target = "gender", source = "gender", qualifiedByName = "getGenderValue")
    @Mapping(target = "age", source = "age")
    UserDto toDto(User user);

    @Mapping(target = "id", ignore = true)
    User toEntity(RegisterUserDto registerUserDto);

    @Named("getGenderValue")
    default String getGenderValue(Gender gender) {
        return gender.name();
    }

}
