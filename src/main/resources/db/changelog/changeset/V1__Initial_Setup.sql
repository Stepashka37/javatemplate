CREATE TABLE users(
    id UUID PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    email VARCHAR(40) NOT NULL,
    password VARCHAR(255) NOT NULL,
    gender VARCHAR NOT NULL,
    age INT NOT NULL
);