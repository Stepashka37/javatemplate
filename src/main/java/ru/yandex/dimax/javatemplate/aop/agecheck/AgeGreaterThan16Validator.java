package ru.yandex.dimax.javatemplate.aop.agecheck;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class AgeGreaterThan16Validator implements ConstraintValidator<AgeGreaterThan16, Integer> {
    @Override
    public boolean isValid(Integer age, ConstraintValidatorContext context) {
        if (age == null) {
            return true; // Null values are handled by @NotNull or other relevant constraints
        }
        return age > 16;
    }
}
