package ru.yandex.dimax.javatemplate.serivce;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.yandex.dimax.javatemplate.aop.exectime.LogExecutionTime;
import ru.yandex.dimax.javatemplate.dto.RegisterUserDto;
import ru.yandex.dimax.javatemplate.dto.UserDto;
import ru.yandex.dimax.javatemplate.exception.EntityNotFoundException;
import ru.yandex.dimax.javatemplate.mapper.UserMapper;
import ru.yandex.dimax.javatemplate.model.User;
import ru.yandex.dimax.javatemplate.repository.UserRepository;

import java.util.UUID;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserService {

    private static final String MODEL_NAME = "User";

    private final UserRepository userRepository;
    private final UserMapper userMapper;

    @LogExecutionTime
    public UserDto getUserById(UUID userId) {
        log.info("Getting %s entity by id=%s".formatted(MODEL_NAME, userId));

        User user = userRepository.findById(userId)
                .orElseThrow(() -> new EntityNotFoundException("%s entity with id=%s not found"
                        .formatted(MODEL_NAME, userId))
                );

        log.info("Successfully retrieved %s entity by id=%s".formatted(MODEL_NAME, userId));

        UserDto userDto = userMapper.toDto(user);

        return userDto;
    }

    @LogExecutionTime
    public UserDto deleteUser(UUID id) {
        return null;
    }

    @LogExecutionTime
    public UserDto createUser(RegisterUserDto registerUserDto) {
        log.info("Creating a new user");

        User user = userRepository.save(
                userMapper.toEntity(registerUserDto)
        );

        log.info("Successfully created new user with id=%s".format(String.valueOf(user.getId())));

        return userMapper.toDto(user);
    }

    @LogExecutionTime
    public UserDto updateUser(UserDto userDto) {
        return null;
    }
}
