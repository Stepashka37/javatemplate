package ru.yandex.dimax.javatemplate.service;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.annotation.Profile;
import ru.yandex.dimax.javatemplate.dto.UserDto;
import ru.yandex.dimax.javatemplate.mapper.UserMapper;
import ru.yandex.dimax.javatemplate.model.Gender;
import ru.yandex.dimax.javatemplate.model.User;
import ru.yandex.dimax.javatemplate.repository.UserRepository;
import ru.yandex.dimax.javatemplate.serivce.UserService;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserService userService;

    @Spy
    private UserMapper mapper = Mappers.getMapper(UserMapper.class);

    @DisplayName("Получение пользователя по ID")
    @Test
    public void getUserByIdTest() {
        // given
        User user = User.builder()
                .id(UUID.randomUUID())
                .age(18)
                .email("email@email.com")
                .name("Name")
                .gender(Gender.MALE)
                .build();
        // when
        when(userRepository.findById(any())).thenReturn(Optional.ofNullable(user));
        // then
        UserDto dto = userService.getUserById(any());

        assertNotNull(dto);
        assertEquals(user.getEmail(), dto.getEmail());
    }
}
