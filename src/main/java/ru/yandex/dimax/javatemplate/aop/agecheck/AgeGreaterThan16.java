package ru.yandex.dimax.javatemplate.aop.agecheck;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.CLASS)
public @interface AgeGreaterThan16 {
    String message() default "Age must be greater than 16";
}
