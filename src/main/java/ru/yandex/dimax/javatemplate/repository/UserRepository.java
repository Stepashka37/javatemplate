package ru.yandex.dimax.javatemplate.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.yandex.dimax.javatemplate.model.User;

import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {
}
