package ru.yandex.dimax.javatemplate.model;

public enum Gender {

    MALE("Мужской"),
    FEMALE("Женский");

    private final String value;

    private Gender(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
